%if 0%{?fedora}
%global buildforkernels akmod
%global debug_package %{nil}
%endif

%global repo rpmfusion

Name:		rtw88-kmod
Version:	20221206
Release:	1%{?dist}
Summary:	"Realtek Kernel modules built and packaged from rtw88 GitHub repo by lwfinger."

License:	GNU
URL:		https://github.com/lwfinger/rtw88
Source0:	https://github.com/lwfinger/rtw88/archive/refs/heads/master.tar.gz

ExclusiveArch:	i586 i686 x86_64 ppc ppc64
BuildRequires:	%{_bindir}/kmodtool
%{!?kernels:BuildRequires: buildsys-build-%{repo}-kerneldevpkgs-%{?buildforkernels:%{buildforkernels}}%{!?buildforkernels:current}-%{_target_cpu} }
Provides:	rtw_pci.ko = %{version}-%{release}
Provides:	rtw_core.ko = %{version}-%{release}
Provides:	rtw_8822ce.ko = %{version}-%{release}
Provides:	rtw_8822c.ko = %{version}-%{release}
Provides:	rtw_8822be.ko = %{version}-%{release}
Provides:	rtw_8822b.ko = %{version}-%{release}
Provides:	rtw_8821ce.ko = %{version}-%{release}
Provides:	rtw_8821c.ko = %{version}-%{release}
Provides:	rtw_8723de.ko = %{version}-%{release}
Provides:	rtw_8723d.ko = %{version}-%{release}

# kmodtool does its magic here
%{expand:%(kmodtool --target %{_target_cpu} --repo rpmfusion --kmodname %{name} %{?buildforkernels:--%{buildforkernels}} %{?kernels:--for-kernels "%{?kernels}"} 2>/dev/null) }

%description
%{summary}

%prep
curl --location --fail --silent --output %{SOURCE0} https://github.com/lwfinger/rtw88/archive/refs/heads/master.tar.gz

# Error out if there was something wrong with kmodtool
%{?kmodtool_check}

# print kmodtool output for debugging purposes:
kmodtool  --target %{_target_cpu}  --repo rpmfusion --kmodname %{name} %{?buildforkernels:--%{buildforkernels}} %{?kernels:--for-kernels "%{?kernels}"} 2>/dev/null

%setup -q -c -n rtw88-master
for kernel_version in %{?kernel_versions} ; do
	echo "The builddir for ${kernel_version%%___*} is ${kernel_version##*__}"
	cp -fr rtw88-main/* _kmod_build_${kernel_version%%___*}
done

%build
for kernel_version in %{?kernel_versions}; do
	%make_build -C ${kernel_version##*___} modules M=$PWD/_kmod_build_${kernel_version%%___*}
done

%install
for kernel_version in %{?kernel_versions}; do
	mkdir -p %{buildroot}%{kmodinstdir_prefix}/${kernel_version%%___*}/%{kmodinstdir_postfix}/
	install -D -m 755 _kmod_build_${kernel_version%%___*}/rtw_pci.ko %{buildroot}%{kmodinstdir_prefix}/${kernel_version%%___*}/%{kmodinstdir_postfix}/
	install -D -m 755 _kmod_build_${kernel_version%%___*}/rtw_core.ko %{buildroot}%{kmodinstdir_prefix}/${kernel_version%%___*}/%{kmodinstdir_postfix}/
	install -D -m 755 _kmod_build_${kernel_version%%___*}/rtw_8822ce.ko %{buildroot}%{kmodinstdir_prefix}/${kernel_version%%___*}/%{kmodinstdir_postfix}/
	install -D -m 755 _kmod_build_${kernel_version%%___*}/rtw_8822c.ko %{buildroot}%{kmodinstdir_prefix}/${kernel_version%%___*}/%{kmodinstdir_postfix}/
	install -D -m 755 _kmod_build_${kernel_version%%___*}/rtw_8822be.ko %{buildroot}%{kmodinstdir_prefix}/${kernel_version%%___*}/%{kmodinstdir_postfix}/
	install -D -m 755 _kmod_build_${kernel_version%%___*}/rtw_8822b.ko %{buildroot}%{kmodinstdir_prefix}/${kernel_version%%___*}/%{kmodinstdir_postfix}/
	install -D -m 755 _kmod_build_${kernel_version%%___*}/rtw_8821ce.ko %{buildroot}%{kmodinstdir_prefix}/${kernel_version%%___*}/%{kmodinstdir_postfix}/
	install -D -m 755 _kmod_build_${kernel_version%%___*}/rtw_8821c.ko %{buildroot}%{kmodinstdir_prefix}/${kernel_version%%___*}/%{kmodinstdir_postfix}/
	install -D -m 755 _kmod_build_${kernel_version%%___*}/rtw_8723de.ko %{buildroot}%{kmodinstdir_prefix}/${kernel_version%%___*}/%{kmodinstdir_postfix}/
	install -D -m 755 _kmod_build_${kernel_version%%___*}/rtw_8723d.ko %{buildroot}%{kmodinstdir_prefix}/${kernel_version%%___*}/%{kmodinstdir_postfix}/
done
%{?akmod_install}

%changelog
* Tue Dec 06 2022 Karlis Kavacis karlis.kavacis@protonmail.com
- Versioning change from Git commit shasum to date of RPM SPEC file update

* Mon Dec 05 2022 Karlis Kavacis karlis.kavacis@protonmail.com
- Initial RPM Build based on rtw88 sources by lwfinger
