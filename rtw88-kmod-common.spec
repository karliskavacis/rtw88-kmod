%global debug_package %{nil}

Name:		rtw88-kmod-common
Version:	20221206
Release:	1%{?dist}
Summary:	"Common files for rtw88-kmod package."

License:	GNU
URL:		https://github.com/lwfinger/rtw88
Source0:	https://gitlab.com/karliskavacis/rtw88-kmod/-/archive/main/rtw88-kmod-main.tar.gz

Requires:	rtw88-kmod >= %{version}-%{release}
Provides:	%{name} = %{version}-%{release}
Provides:	%{name}_blacklist.conf = %{version}-%{release}

%description
%{summary}

%prep
curl --location --remote-time --show-error --fail --output %{SOURCE0} https://gitlab.com/karliskavacis/rtw88-kmod/-/archive/main/rtw88-kmod-main.tar.gz

%setup -q -c -n rtw88-kmod-main

%install
mkdir -p %{buildroot}/usr/lib/modprobe.d/
install -m 0644 rtw88-kmod-main/%{name}_blacklist.conf %{buildroot}/usr/lib/modprobe.d/%{name}_blacklist.conf

%files
%defattr(-,root,root,-)
/usr/lib/modprobe.d/%{name}_blacklist.conf

%changelog
* Tue Dec 06 2022 Karlis Kavacis karlis.kavacis@protonmail.com
- Versioning change from Git commit shasum to date of RPM SPEC file update

* Mon Dec 05 2022 Karlis Kavacis karlis.kavacis@protonmail.com
- Initial RPM Build based on rtw88 sources by lwfinger
